import firebase from "firebase";

// Firebase is crap for pagination

class FirebaseService {

    db = null;
    currentSnap = null;
    
    constructor(){
        this.db = firebase.firestore();
        const settings = {timestampsInSnapshots: true};
        this.db.settings(settings);
    }

    createTeam(teamName, teamCountry){
        const team = {
            id: 'E32E038MC',
            name: teamName,
            countryCode: teamCountry
        };
        this.db.collection("Teams").doc(teamName.toString()).set(team);
    }


    getTeams(){
        return new Promise((resolve, reject) => {
            let teamsRef = this.db.collection('Teams').orderBy('name');
            teamsRef.get()
            .then(snapshot => {
                this.currentSnap = snapshot.docs;
                resolve(snapshot.size);
            })
            .catch(err => {
                console.log('Error getting documents', err);
                resolve(null);
            });
        });
    }

    getPagination(currentPage = 1, limit, order = null){
        return new Promise((resolve, reject) => {
            let data = [];
            const startElement = limit * (currentPage -1);
            let teamsRef = this.db.collection('Teams').orderBy(order !== null ? order : 'name').startAt(this.currentSnap[startElement]).limit(limit);
            teamsRef.get()
            .then(snapshot => {
                snapshot.forEach(doc =>{
                    data.push(doc.data());
                });
                resolve(data);
            })
            .catch(err => {
                console.log('Error getting documents', err);
                resolve(null);
            });
        });
    }

    searchQuery(query){
        return new Promise((resolve, reject) => {
            let data = [];
            let teamsRef = this.db.collection('Teams').where('name', '==', query);
            teamsRef.get()
            .then(snapshot => {
                snapshot.forEach(doc =>{
                    data.push(doc.data());
                });
                resolve(data);
            })
            .catch(err => {
                console.log('Error getting documents', err);
                resolve(null);
            });
        });
    }
 
  

}

export default FirebaseService;
