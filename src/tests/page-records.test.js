import React from 'react';
import renderer from 'react-test-renderer';
import RecordsPage from '../pages/page-records/page-records';
import { firebaseConfig } from '../firebase/firebase-config';
import firebase from "firebase";
const fire = firebase.initializeApp(firebaseConfig);

it('RecordsPage Renders without crashing.', () => {
    const component = renderer.create(
       <RecordsPage></RecordsPage> ,
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  
});


afterAll(() => {
    fire.delete();
  });