import React from 'react';
import renderer from 'react-test-renderer';
import Navbar  from '../components/navbar/navbar';
import { BrowserRouter as Router } from "react-router-dom";


it('Navbar Renders without crashing.', () => {
    const component = renderer.create(
        <Router><Navbar></Navbar></Router> ,
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});