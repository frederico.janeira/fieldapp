import React from 'react';
import renderer from 'react-test-renderer';
import TeamList from '../components/team-list/team-list';

const fakeData = [
    {id: '2131E9E0F0', name: 'TeamWin', countryCode: 'PT'},
    {id: '1846E9Z1F0', name: 'Molezitos', countryCode: 'ES'},
    {id: '8593RP9CF0', name: 'Sputnik', countryCode: 'RU'},
];

it('TeamList Renders without crashing.', () => {
    const component = renderer.create(
       <TeamList data={fakeData}></TeamList> ,
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});