import React from 'react';
import renderer from 'react-test-renderer';
import TeamListFilters from '../components/team-list-filters/team-list-filters';

const handleSubmit = () => {};

const handleQueryChange = () => {};

it('TeamListFilters Renders without crashing.', () => {
    const component = renderer.create(
    <TeamListFilters onSubmit={handleSubmit} filterChange={handleQueryChange}></TeamListFilters>    
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});