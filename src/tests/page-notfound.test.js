import React from 'react';
import renderer from 'react-test-renderer';
import NotFoundPage from '../pages/page-notfound/page-notfound';

it('NotFoundPage Renders without crashing.', () => {
    const component = renderer.create(
       <NotFoundPage></NotFoundPage> ,
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
    
});