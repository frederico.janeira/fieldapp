import React from 'react';
import renderer from 'react-test-renderer';
import { firebaseConfig } from '../firebase/firebase-config';
import firebase from "firebase";
const fire = firebase.initializeApp(firebaseConfig);
import HomePage from '../pages/page-home/page-home';

it('HomePage Renders without crashing.', () => {
    const component = renderer.create(
       <HomePage></HomePage> ,
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
    
});


afterAll(() => {
    fire.delete();
});