import React from 'react';
import renderer from 'react-test-renderer';
import TeamForm  from '../components/form-team/form-team';
import { firebaseConfig } from '../firebase/firebase-config';
import firebase from "firebase";
const fire = firebase.initializeApp(firebaseConfig);

it('TeamForm Renders without crashing.', () => {
    const component = renderer.create(
      <TeamForm></TeamForm>,
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();

});



afterAll(() => {
  fire.delete();
});