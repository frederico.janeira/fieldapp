import React, { Component } from 'react';


class NotFoundPage extends Component {
  render() {
    return (
      <div className="App text-light mt-5">
        <h1>404 - Page Not Found</h1>
      </div>
    );
  }
}

export default NotFoundPage;
