import React, { Component } from 'react';
import './page-records.scss';
import TeamList from '../../components/team-list/team-list';
import FirebaseService from '../../firebase/firebase-service';
import Pagination from "react-js-pagination";
import TeamListFilters from '../../components/team-list-filters/team-list-filters';


class RecordsPage extends Component {

  lastPage = 0;
  limit = 2;
  currentFilter = 'name';
  initialTeams = [];
  initialLength = 0;
  firebase = new FirebaseService();


  constructor(props){
    super(props);
    this.state = {
      teams: [],
      activePage : 1,
      dataLength: 0,
    }
    this.handleQueryChange = this.handleQueryChange.bind(this);
    this.handlePageChange = this.handlePageChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    
    this.firebase.getTeams().then(size => {
      this.currentFilter = 'name';
      this.setState({
        dataLength: size
      });
      this.initialLength = size;
      this.firebase.getPagination(this.state.activePage, this.limit).then((data)=> {
        this.setState({
          teams: data,
        });
        this.initialTeams = data
      });
    });
  }

  handleSubmit(query) {
    this.firebase.searchQuery(query).then((data)=>{
      this.setState({
        teams: data,
        dataLength: 1
      });
    });
  }

  handleQueryChange(val){
    if(val.toString().trim() === ''){
      this.setState({
        teams: this.initialTeams,
        dataLength: this.initialLength
      });
    }
  }

  handlePageChange(event){
    const currentPage = event;
    this.firebase.getPagination(currentPage, this.limit, this.currentFilter).then((data)=> {
      this.setState({
        teams: data,
        activePage: currentPage
      });
    });
    this.lastPage = currentPage;
  }

  render() {
      return (
        <div className="App">
            <div className="row mx-0 my-4 ">
            <div className="col-2">
            <TeamListFilters onSearch={this.handleSubmit} filterChange={this.handleQueryChange}></TeamListFilters>
            </div>
              <div className="col-10">
              {this.state.dataLength > 0 ? (
                <div>
                  <TeamList data={this.state.teams}></TeamList>
                  <Pagination
                        prevPageText='Anterior' nextPageText='Seguinte'
                        itemsCountPerPage={this.limit}
                        activePage={this.state.activePage}
                        totalItemsCount={this.state.dataLength}
                        onChange={this.handlePageChange}
                  />
                </div> )
                : (<div className="loader"></div>)}
              </div>
            </div>
        </div>
      );
   
  }
}

export default RecordsPage;
