import React, { Component } from 'react';
import './navbar.scss';
import {Link} from "react-router-dom";


class Navbar extends Component {
  render() {
    return (
        <div>
            <nav className="navbar navbar-expand navbar-dark bg-dark">
            <div className="collapse navbar-collapse show" id="navbarNav">
                <ul className="navbar-nav">
                    <li className="nav-item active">
                        <div className="nav-link"><Link to="/">Create Team</Link></div>
                    </li>
                    <li className="nav-item">
                        <div className="nav-link"><Link to="/records">Team List</Link></div>
                    </li>
                </ul>
            </div>
            </nav>
        </div>
    );
  }
}

export default Navbar;
