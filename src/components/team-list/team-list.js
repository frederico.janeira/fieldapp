import React, { Component } from 'react';
import './team-list.scss';


class TeamList extends Component {

  render() {
      if(this.props.data.length < 0){return (<div className="loader"></div>);}
      else {
        return (
            <div className="teamListContainer">
                <table className="table table-light text-dark">
                    <thead className="thead-dark">
                        <tr>
                            <th scope="col">Id</th>
                            <th scope="col">Name</th>
                            <th scope="col">CountryCode</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.data.map((item, index) => (
                            <tr key={index}>
                                <th>{item.id}</th>
                                <th>{item.name}</th>
                                <th>{item.countryCode}</th>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        );
      }
  }
}

export default TeamList;
