import React, { Component } from 'react';
import FirebaseService from '../../firebase/firebase-service';
import "./form-team.scss";

class FormTeam extends Component {

  firebase = new FirebaseService();

  constructor(props){
    super(props);
    this.state = {
      teamName: '',
      teamCountry: 'PT'
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.firebase.createTeam(this.state.teamName, this.state.teamCountry);
  }

  
  render() {
    return (
      <div className="TeamForm">
        <h3 className="text-light">Create Team</h3>
        <div className="card">
          <div className="card-body">
            <form onSubmit={this.handleSubmit}> 
              <label className="text-dark"><b>Name: </b></label>
              <input type="text" className="form-control form-control-sm" value={this.state.teamName} onChange={this.handleChange} name="teamName" />
              <label className="text-dark"><b>Country: </b></label>
              <select className="form-control" value={this.state.teamCountry} onChange={this.handleChange} name="teamCountry">
                <option value="PT">Portugal</option>
                <option value="ES">Espanha</option>
                <option value="FR">França</option>
              </select>
              <br></br>
              <button type="submit" className="btn btn-sm btn-primary btn-block" value="Submit">Create</button> 
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default FormTeam;