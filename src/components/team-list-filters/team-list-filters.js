import React, { Component } from 'react';
import "./team-list-filters.scss";

class TeamListFilters extends Component {

  constructor(props){
    super(props);
    this.state = {
        searchQuery: ''
    };
    this.handleChange = this.handleChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  handleChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
    this.props.filterChange(value);
  }


  onSubmit(event){
    event.preventDefault();
    this.props.onSearch(this.state.searchQuery);
  }
  

  render() {
    return (
        <div className="bg-light p-2">
        <h2>Search:</h2>
        <div>
          <form onSubmit={this.onSubmit}>
            <div className="d-flex flex-row">
              <input className="form-control" name="searchQuery" onChange={this.handleChange} value={this.state.searchQuery}/>
            </div>
            <div>
              <button className="form-control" type="submit">Search</button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

export default TeamListFilters;