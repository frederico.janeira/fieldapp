import React, { Component } from 'react';
import './App.scss';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import HomePage from './pages/page-home/page-home';
import Navbar from "./components/navbar/navbar"
import NotFoundPage from './pages/page-notfound/page-notfound';
import RecordsPage from './pages/page-records/page-records';
import { firebaseConfig } from './firebase/firebase-config';
import firebase from "firebase";

class App extends Component {
  constructor(props){
    super(props)
    firebase.initializeApp(firebaseConfig);
  }
  
  render() {
    return (
        <Router>
            <div>
              <Navbar></Navbar>
              <Switch>
                <Route exact path="/" component={HomePage} />
                <Route path="/records" component={RecordsPage} />
                <Route component={NotFoundPage} />
              </Switch>
            </div>
        </Router>
    );
  }
}

export default App;


